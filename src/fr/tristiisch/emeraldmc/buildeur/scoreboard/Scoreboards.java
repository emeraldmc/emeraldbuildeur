package fr.tristiisch.emeraldmc.buildeur.scoreboard;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.tristiisch.emeraldmc.api.commons.EmeraldServers;
import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.scoreboard.EmeraldScoreboardSign;
import fr.tristiisch.emeraldmc.api.spigot.scoreboard.ScoreboardList;
import fr.tristiisch.emeraldmc.api.spigot.task.TaskManager;
import fr.tristiisch.emeraldmc.buildeur.Main;

public class Scoreboards {

	static {
		EmeraldScoreboardSign.animation(Main.getInstance(), 0, "play.emeraldmc.fr", ChatColor.GOLD, ChatColor.YELLOW);
	}

	public static void setScoreboards(final Player player, final EmeraldPlayer emeraldPlayer) {
		TaskManager.runTask(() -> {
			final EmeraldScoreboardSign emeraldScoreboard = new EmeraldScoreboardSign("&2EmeraldMC", player);
			emeraldScoreboard.create();
			updateScoreboards(player, emeraldPlayer);
		});
	}

	public static void setScoreboards(final Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			setScoreboards(player, new AccountProvider(player.getUniqueId()).getEmeraldPlayer());
		});
	}

	public static void updateScoreboards(final UUID uuid) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			updateScoreboards(Bukkit.getPlayer(uuid), new AccountProvider(uuid).getEmeraldPlayer());
		});
	}

	public static void updateScoreboards(final EmeraldPlayer emeraldPlayer) {
		updateScoreboards(Bukkit.getPlayer(emeraldPlayer.getUniqueId()), emeraldPlayer);
	}

	public static void updateScoreboards(final Player player) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			updateScoreboards(player, new AccountProvider(player.getUniqueId()).getEmeraldPlayer());
		});
	}

	public static void removeScoreboard(final Player player) {
		EmeraldScoreboardSign.removeScoreboard(player);
	}

	public static void updateConnected(final Player player, final int connected) {
		final EmeraldScoreboardSign emeraldScoreboard = EmeraldScoreboardSign.getScoreboard(player);
		if(emeraldScoreboard == null) {
			return;
		}
		emeraldScoreboard.setLine(2, "&7Connecté" + Utils.withOrWithoutS(connected) + ": &a" + connected);
		emeraldScoreboard.updateLines();
	}

	public static void updateScoreboards(final Player player, final EmeraldPlayer emeraldPlayer) {

		TaskManager.runTask(() -> {
			ScoreboardList.setNameTag(player, Utils.getOppositeCharForNumber(emeraldPlayer.getGroup().getID()), emeraldPlayer.getGroup().getPrefix());

			final EmeraldScoreboardSign emeraldScoreboard = EmeraldScoreboardSign.getScoreboard(player);
			if(emeraldScoreboard == null) {
				return;
			}
			emeraldScoreboard.clearLines();
			emeraldScoreboard.addLine("");
			emeraldScoreboard.addLine("&7Pseudo: &a" + emeraldPlayer.getName());
			emeraldScoreboard.addLine("&7Grade" + Utils.withOrWithoutS(emeraldPlayer.getGroupsName().size()) + ": &a" + String.join(", ", emeraldPlayer.getGroupsName()));
			emeraldScoreboard.addLine("");
			emeraldScoreboard.addLine("&7Pierre" + Utils.withOrWithoutS(emeraldPlayer.getPierres()) + ": &a" + emeraldPlayer.getPierres());
			emeraldScoreboard.addLine("&7Minerais: &a" + emeraldPlayer.getMinerais());
			emeraldScoreboard.addLine("");

			final int connected = EmeraldServers.getOnlineConnected();
			final String sconnected = EmeraldServers.getOnlineConnected() == 0 ? String.valueOf(connected) : "";
			emeraldScoreboard.addLine("&7Connecté" + Utils.withOrWithoutS(EmeraldServers.getOnlineConnected()) + ": &a" + sconnected);
			emeraldScoreboard.addLine("");
			emeraldScoreboard.addLine("&6play.emeraldmc.fr");
			emeraldScoreboard.reverseLines();
			emeraldScoreboard.updateLines();
		});
	}
}
