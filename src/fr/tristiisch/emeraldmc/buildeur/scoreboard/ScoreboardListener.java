package fr.tristiisch.emeraldmc.buildeur.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerChangeEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerLoadEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.PlayerScoreboardReloadEvent;
import fr.tristiisch.emeraldmc.api.spigot.customevent.EmeraldServerInfoEvent;

public class ScoreboardListener implements Listener {

	@EventHandler
	public void AsyncEmeraldPlayerChange(final AsyncEmeraldPlayerChangeEvent event) {
		final Player player = event.getPlayer();
		final EmeraldPlayer emeraldPlayer = event.getEmeraldPlayer();

		Scoreboards.updateScoreboards(player, emeraldPlayer);
	}

	@EventHandler
	public void AsyncEmeraldPlayerLoad(final AsyncEmeraldPlayerLoadEvent event) {
		final Player player = event.getPlayer();
		final EmeraldPlayer emeraldPlayer = event.getEmeraldPlayer();
		Scoreboards.setScoreboards(player, emeraldPlayer);
	}

	@EventHandler
	public void AsyncPlayerScoreboardReload(final PlayerScoreboardReloadEvent event) {
		final Player player = event.getPlayer();
		Scoreboards.updateScoreboards(player);
	}

	@EventHandler
	public void EmeraldServerInfoEvent(final EmeraldServerInfoEvent event) {
		for(final Player player : Bukkit.getOnlinePlayers()) {
			Scoreboards.updateConnected(player, event.getEmeraldServersOnline());
		}
	}
}
