package fr.tristiisch.emeraldmc.buildeur.gui;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;


public class EventGui implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	private void BlockPlaceEvent(BlockPlaceEvent e) {
		ItemStack block = e.getPlayer().getItemInHand();
		List<String> Llore = block.getItemMeta().getLore();
		if(Llore != null) {
			String lore = "";
			for(String lores : Llore) {
				lore += lores;
			}
			if(lore.matches("^\\d+:\\d+$")) {
				String[] Slore = lore.split(":");
				int ID = Integer.parseInt(Slore[0]);
				byte metaID = Byte.parseByte(Slore[1]);
				e.getBlock().setType(Material.getMaterial(ID));
				e.getBlock().setData(metaID);
			}
		}

	}

}
