package fr.tristiisch.emeraldmc.buildeur.gui;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.datamanagment.redis.AccountProvider;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.buildeur.Main;

public class CommandGui implements CommandExecutor {

	private final int SizeGUI = 5*9;
	private final ArrayList<ItemStack> items = new ArrayList<>();
	private final Main main = Main.getInstance();

	public CommandGui() {
		this.addItem(Material.LOG, 1, (short) 0, "§6Bois de Chêne sans tronc", "17:12");
		this.addItem(Material.LOG, 1, (short) 1, "§6Bois de Sapin sans tronc", "17:13");
		this.addItem(Material.LOG, 1, (short) 2, "§6Bois de Bouleau sans tronc", "17:14");
		this.addItem(Material.LOG, 1, (short) 3, "§6Bois de Jungle sans tronc", "17:15");
		this.addItem(Material.BARRIER, 1, (short) 0, "§6Barrier", null);
		this.addItem(Material.HUGE_MUSHROOM_1, 1, (short) 0, "§6Champi marron", null);
		this.addItem(Material.HUGE_MUSHROOM_2, 1, (short) 0, "§6Champi rouge", null);
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(EmeraldSpigot.getInstance(), () -> {
			if(sender instanceof Player) {
				final Player player = (Player) sender;
				final EmeraldPlayer user = new AccountProvider(player.getUniqueId()).getEmeraldPlayer();
				if(user.hasPower(45, 65, 75)) {
					final Inventory inventory = Bukkit.createInventory(null, this.SizeGUI , ChatColor.GOLD + "Gui buildeur");
					int i = 0;
					for(final ItemStack item : this.items) {
						inventory.setItem(i, item);
						i++;
					}
					((Player) sender).openInventory(inventory);
				} else {
					player.sendMessage(Utils.color(this.main.getConfig().getString("commun.messages.noperm")));
				}
			} else {
				sender.sendMessage(Utils.color(this.main.getConfig().getString("spigot.commands.build.cantconsole")));
			}
		});
		return true;
	}


	private void addItem(final Material material, final int number, final short id, final String name, final String Slore) {
		final ItemStack itemstack = new ItemStack(material, number, id);
		final ItemMeta itemmeta = itemstack.getItemMeta();
		if(Slore != null) {
			itemmeta.setLore(Arrays.asList(Slore.split("\n")));
		}
		if(name != null) {
			itemmeta.setDisplayName(name);
		}
		itemstack.setItemMeta(itemmeta);
		this.items.add(itemstack);
	}
}
