package fr.tristiisch.emeraldmc.buildeur.listeners;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.Team;

import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldPlayer;
import fr.tristiisch.emeraldmc.api.spigot.customevent.AsyncEmeraldPlayerLoadEvent;
import fr.tristiisch.emeraldmc.api.spigot.title.Title;
import fr.tristiisch.emeraldmc.api.spigot.utils.SpigotUtils;
import fr.tristiisch.emeraldmc.buildeur.Main;
import fr.tristiisch.emeraldmc.buildeur.scoreboard.Scoreboards;

public class JoinListener implements Listener {

	@EventHandler
	public void PlayerJoinEvent(final PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		player.setOp(false);
		Title.sendTitle(player, "&2EmeraldMC", "&7Serveur Buildeurs", 20, 40, 20);
		init(player);
	}

	@EventHandler
	public void AsyncEmeraldPlayerLoad(final AsyncEmeraldPlayerLoadEvent event) {
		final Player player = event.getPlayer();
		final EmeraldPlayer emeraldPlayer = event.getEmeraldPlayer();
		if(emeraldPlayer.hasPower(EmeraldGroup.BUILDEUR)) {
			final Plugin plugin = Main.getInstance();
			final PermissionAttachment attachment = player.addAttachment(plugin);
			attachment.setPermission("worldedit.*", true);
			attachment.setPermission("essentials.*", true);
			attachment.setPermission("heads.*", true);
			attachment.setPermission("voxelsniper.*", true);
			attachment.setPermission("multiverse.*", true);
		} else if(emeraldPlayer.hasPower(EmeraldGroup.RESPBUILD, EmeraldGroup.ADMIN)) {
			player.setOp(true);
		}
		Scoreboards.setScoreboards(player, emeraldPlayer);
	}

	@EventHandler
	public void PlayerQuitEvent(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		player.setOp(false);

		for(final Team team : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()) {
			team.removeEntry(player.getName());
		}
		Scoreboards.removeScoreboard(player);
	}

	public static void init(final Player player) {
		player.setGameMode(GameMode.CREATIVE);
		player.setWalkSpeed(0.2f);
		player.setAllowFlight(true);
		player.setFlying(false);
		player.setFlySpeed(0.5f);
		player.setFoodLevel(20);
		player.setFlying(false);
		SpigotUtils.addNightVision(player);
	}
}
