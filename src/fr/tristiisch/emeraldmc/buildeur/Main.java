package fr.tristiisch.emeraldmc.buildeur;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.tristiisch.emeraldmc.api.commons.Utils;
import fr.tristiisch.emeraldmc.api.commons.object.EmeraldGroup;
import fr.tristiisch.emeraldmc.api.spigot.EmeraldSpigot;
import fr.tristiisch.emeraldmc.api.spigot.core.build.BuildCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.commands.GamemodeCommand;
import fr.tristiisch.emeraldmc.api.spigot.core.listeners.TchatFormat;
import fr.tristiisch.emeraldmc.buildeur.gui.CommandGui;
import fr.tristiisch.emeraldmc.buildeur.gui.EventGui;
import fr.tristiisch.emeraldmc.buildeur.listeners.JoinListener;
import fr.tristiisch.emeraldmc.buildeur.scoreboard.ScoreboardListener;

public class Main extends JavaPlugin {

	public static Main instance;

	@Override
	public void onEnable() {
		instance = this;

		final PluginManager pluginmanager = this.getServer().getPluginManager();

		// Gestion Commands
		this.getCommand("gui").setExecutor(new CommandGui());

		// Gestion Events
		pluginmanager.registerEvents(new EventGui(), this);
		pluginmanager.registerEvents(new TchatFormat(), this);
		pluginmanager.registerEvents(new ScoreboardListener(), this);
		pluginmanager.registerEvents(new JoinListener(), this);

		Bukkit.getServer().setWhitelist(true);
		// Custom power
		BuildCommand.setPower(45, 65, 75);
		GamemodeCommand.setPower(EmeraldGroup.ADMIN, EmeraldGroup.RESPMODO, EmeraldGroup.RESPBUILD, EmeraldGroup.BUILDEUR);

		for(final Player player : this.getServer().getOnlinePlayers()) {
			JoinListener.init(player);
		}
		final ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(Utils.color("&a" + this.getDescription().getName() + " &7(V" + this.getDescription().getVersion() + ")"));
	}

	@Override
	public void onDisable() {

	}

	@Override
	public FileConfiguration getConfig() {
		return getPlugin(EmeraldSpigot.class).getConfig();
	}

	public static Main getInstance() {
		return instance;
	}
}
